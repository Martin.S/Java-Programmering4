import java.util.Scanner;

public class SnuTekst {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		
		System.out.println("Skriv in String: ");
		String myString = scan.next();
		System.out.println("String baklengs er: " + recursiveReverse(myString));
		
		
	}
	public static String recursiveReverse(String reverseString) {
	    if ((reverseString == null) || (reverseString.length() <= 1)) {
	        return reverseString;
	    }
	    int num = 1;
	    return recursiveReverse(reverseString.substring(1)) + reverseString.charAt(0);
	}
}
